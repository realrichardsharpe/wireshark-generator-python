# The class for the LUA dissector generator

import sys

class GenPacketTool:

    def __init__(self, log_it, parent_ast, verbose_level=0, outfile=sys.stdout):
        self.outfile = outfile

    def code(self, code_line):
        self.outfile.write(code_line)
        self.outfile.write('\n')

    def generateCode(self):
        self.code('/* No code generation yet */')
