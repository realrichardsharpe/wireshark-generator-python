# Generated from ../grammar/WiresharkGenerator.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2,")
        buf.write("\u0130\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\3\2\3\2\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\b")
        buf.write("\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3")
        buf.write("\t\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write("\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\22\3\22\3\23")
        buf.write("\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\25\3\25")
        buf.write("\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30")
        buf.write("\3\30\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\33\3\34\3\34")
        buf.write("\3\34\3\35\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3\37\3 ")
        buf.write("\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3%\3%\3&\3&\3&")
        buf.write("\3&\3&\3&\3&\3\'\3\'\7\'\u0102\n\'\f\'\16\'\u0105\13\'")
        buf.write("\3\'\3\'\3(\3(\7(\u010b\n(\f(\16(\u010e\13(\3(\3(\3(\3")
        buf.write("(\3)\3)\7)\u0116\n)\f)\16)\u0119\13)\3*\6*\u011c\n*\r")
        buf.write("*\16*\u011d\3*\3*\3+\3+\3+\3+\6+\u0126\n+\r+\16+\u0127")
        buf.write("\3+\6+\u012b\n+\r+\16+\u012c\5+\u012f\n+\4\u0103\u010c")
        buf.write("\2,\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r")
        buf.write("\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30")
        buf.write("/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'")
        buf.write("M(O)Q*S+U,\3\2\b\4\2\f\f\17\17\4\2C\\c|\6\2\62;C\\aac")
        buf.write("|\5\2\13\f\17\17\"\"\5\2\62;CHch\3\2\62;\2\u0136\2\3\3")
        buf.write("\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2")
        buf.write("\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2")
        buf.write("\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2")
        buf.write("\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2")
        buf.write("\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3")
        buf.write("\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2")
        buf.write("\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3")
        buf.write("\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K")
        buf.write("\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2")
        buf.write("U\3\2\2\2\3W\3\2\2\2\5Y\3\2\2\2\7`\3\2\2\2\tb\3\2\2\2")
        buf.write("\13q\3\2\2\2\rs\3\2\2\2\17u\3\2\2\2\21w\3\2\2\2\23\u0084")
        buf.write("\3\2\2\2\25\u0086\3\2\2\2\27\u0088\3\2\2\2\31\u0097\3")
        buf.write("\2\2\2\33\u0099\3\2\2\2\35\u00a1\3\2\2\2\37\u00a6\3\2")
        buf.write("\2\2!\u00ae\3\2\2\2#\u00b0\3\2\2\2%\u00b2\3\2\2\2\'\u00b7")
        buf.write("\3\2\2\2)\u00bc\3\2\2\2+\u00c3\3\2\2\2-\u00ca\3\2\2\2")
        buf.write("/\u00d1\3\2\2\2\61\u00d5\3\2\2\2\63\u00d7\3\2\2\2\65\u00da")
        buf.write("\3\2\2\2\67\u00dd\3\2\2\29\u00e0\3\2\2\2;\u00e3\3\2\2")
        buf.write("\2=\u00e6\3\2\2\2?\u00e9\3\2\2\2A\u00ec\3\2\2\2C\u00ee")
        buf.write("\3\2\2\2E\u00f0\3\2\2\2G\u00f2\3\2\2\2I\u00f4\3\2\2\2")
        buf.write("K\u00f8\3\2\2\2M\u00ff\3\2\2\2O\u0108\3\2\2\2Q\u0113\3")
        buf.write("\2\2\2S\u011b\3\2\2\2U\u012e\3\2\2\2WX\7=\2\2X\4\3\2\2")
        buf.write("\2YZ\7g\2\2Z[\7p\2\2[\\\7f\2\2\\]\7k\2\2]^\7c\2\2^_\7")
        buf.write("p\2\2_\6\3\2\2\2`a\7?\2\2a\b\3\2\2\2bc\7f\2\2cd\7k\2\2")
        buf.write("de\7u\2\2ef\7u\2\2fg\7g\2\2gh\7e\2\2hi\7v\2\2ij\7q\2\2")
        buf.write("jk\7t\2\2kl\7V\2\2lm\7c\2\2mn\7d\2\2no\7n\2\2op\7g\2\2")
        buf.write("p\n\3\2\2\2qr\7]\2\2r\f\3\2\2\2st\7.\2\2t\16\3\2\2\2u")
        buf.write("v\7_\2\2v\20\3\2\2\2wx\7r\2\2xy\7t\2\2yz\7q\2\2z{\7v\2")
        buf.write("\2{|\7q\2\2|}\7F\2\2}~\7g\2\2~\177\7v\2\2\177\u0080\7")
        buf.write("c\2\2\u0080\u0081\7k\2\2\u0081\u0082\7n\2\2\u0082\u0083")
        buf.write("\7u\2\2\u0083\22\3\2\2\2\u0084\u0085\7}\2\2\u0085\24\3")
        buf.write("\2\2\2\u0086\u0087\7\177\2\2\u0087\26\3\2\2\2\u0088\u0089")
        buf.write("\7f\2\2\u0089\u008a\7k\2\2\u008a\u008b\7u\2\2\u008b\u008c")
        buf.write("\7u\2\2\u008c\u008d\7g\2\2\u008d\u008e\7e\2\2\u008e\u008f")
        buf.write("\7v\2\2\u008f\u0090\7q\2\2\u0090\u0091\7t\2\2\u0091\u0092")
        buf.write("\7G\2\2\u0092\u0093\7p\2\2\u0093\u0094\7v\2\2\u0094\u0095")
        buf.write("\7t\2\2\u0095\u0096\7{\2\2\u0096\30\3\2\2\2\u0097\u0098")
        buf.write("\7<\2\2\u0098\32\3\2\2\2\u0099\u009a\7f\2\2\u009a\u009b")
        buf.write("\7g\2\2\u009b\u009c\7h\2\2\u009c\u009d\7c\2\2\u009d\u009e")
        buf.write("\7w\2\2\u009e\u009f\7n\2\2\u009f\u00a0\7v\2\2\u00a0\34")
        buf.write("\3\2\2\2\u00a1\u00a2\7g\2\2\u00a2\u00a3\7p\2\2\u00a3\u00a4")
        buf.write("\7w\2\2\u00a4\u00a5\7o\2\2\u00a5\36\3\2\2\2\u00a6\u00a7")
        buf.write("\7v\2\2\u00a7\u00a8\7{\2\2\u00a8\u00a9\7r\2\2\u00a9\u00aa")
        buf.write("\7g\2\2\u00aa\u00ab\7f\2\2\u00ab\u00ac\7g\2\2\u00ac\u00ad")
        buf.write("\7h\2\2\u00ad \3\2\2\2\u00ae\u00af\7*\2\2\u00af\"\3\2")
        buf.write("\2\2\u00b0\u00b1\7+\2\2\u00b1$\3\2\2\2\u00b2\u00b3\7x")
        buf.write("\2\2\u00b3\u00b4\7q\2\2\u00b4\u00b5\7k\2\2\u00b5\u00b6")
        buf.write("\7f\2\2\u00b6&\3\2\2\2\u00b7\u00b8\7e\2\2\u00b8\u00b9")
        buf.write("\7c\2\2\u00b9\u00ba\7u\2\2\u00ba\u00bb\7g\2\2\u00bb(\3")
        buf.write("\2\2\2\u00bc\u00bd\7u\2\2\u00bd\u00be\7y\2\2\u00be\u00bf")
        buf.write("\7k\2\2\u00bf\u00c0\7v\2\2\u00c0\u00c1\7e\2\2\u00c1\u00c2")
        buf.write("\7j\2\2\u00c2*\3\2\2\2\u00c3\u00c4\7g\2\2\u00c4\u00c5")
        buf.write("\7z\2\2\u00c5\u00c6\7v\2\2\u00c6\u00c7\7g\2\2\u00c7\u00c8")
        buf.write("\7t\2\2\u00c8\u00c9\7p\2\2\u00c9,\3\2\2\2\u00ca\u00cb")
        buf.write("\7u\2\2\u00cb\u00cc\7v\2\2\u00cc\u00cd\7t\2\2\u00cd\u00ce")
        buf.write("\7w\2\2\u00ce\u00cf\7e\2\2\u00cf\u00d0\7v\2\2\u00d0.\3")
        buf.write("\2\2\2\u00d1\u00d2\7\60\2\2\u00d2\u00d3\7\60\2\2\u00d3")
        buf.write("\u00d4\7\61\2\2\u00d4\60\3\2\2\2\u00d5\u00d6\7\61\2\2")
        buf.write("\u00d6\62\3\2\2\2\u00d7\u00d8\7\60\2\2\u00d8\u00d9\7\61")
        buf.write("\2\2\u00d9\64\3\2\2\2\u00da\u00db\7#\2\2\u00db\u00dc\7")
        buf.write("?\2\2\u00dc\66\3\2\2\2\u00dd\u00de\7@\2\2\u00de\u00df")
        buf.write("\7?\2\2\u00df8\3\2\2\2\u00e0\u00e1\7>\2\2\u00e1\u00e2")
        buf.write("\7?\2\2\u00e2:\3\2\2\2\u00e3\u00e4\7?\2\2\u00e4\u00e5")
        buf.write("\7?\2\2\u00e5<\3\2\2\2\u00e6\u00e7\7>\2\2\u00e7\u00e8")
        buf.write("\7>\2\2\u00e8>\3\2\2\2\u00e9\u00ea\7@\2\2\u00ea\u00eb")
        buf.write("\7@\2\2\u00eb@\3\2\2\2\u00ec\u00ed\7/\2\2\u00edB\3\2\2")
        buf.write("\2\u00ee\u00ef\7-\2\2\u00efD\3\2\2\2\u00f0\u00f1\7,\2")
        buf.write("\2\u00f1F\3\2\2\2\u00f2\u00f3\7(\2\2\u00f3H\3\2\2\2\u00f4")
        buf.write("\u00f5\7d\2\2\u00f5\u00f6\7k\2\2\u00f6\u00f7\7i\2\2\u00f7")
        buf.write("J\3\2\2\2\u00f8\u00f9\7n\2\2\u00f9\u00fa\7k\2\2\u00fa")
        buf.write("\u00fb\7v\2\2\u00fb\u00fc\7v\2\2\u00fc\u00fd\7n\2\2\u00fd")
        buf.write("\u00fe\7g\2\2\u00feL\3\2\2\2\u00ff\u0103\7$\2\2\u0100")
        buf.write("\u0102\13\2\2\2\u0101\u0100\3\2\2\2\u0102\u0105\3\2\2")
        buf.write("\2\u0103\u0104\3\2\2\2\u0103\u0101\3\2\2\2\u0104\u0106")
        buf.write("\3\2\2\2\u0105\u0103\3\2\2\2\u0106\u0107\7$\2\2\u0107")
        buf.write("N\3\2\2\2\u0108\u010c\7%\2\2\u0109\u010b\13\2\2\2\u010a")
        buf.write("\u0109\3\2\2\2\u010b\u010e\3\2\2\2\u010c\u010d\3\2\2\2")
        buf.write("\u010c\u010a\3\2\2\2\u010d\u010f\3\2\2\2\u010e\u010c\3")
        buf.write("\2\2\2\u010f\u0110\t\2\2\2\u0110\u0111\3\2\2\2\u0111\u0112")
        buf.write("\b(\2\2\u0112P\3\2\2\2\u0113\u0117\t\3\2\2\u0114\u0116")
        buf.write("\t\4\2\2\u0115\u0114\3\2\2\2\u0116\u0119\3\2\2\2\u0117")
        buf.write("\u0115\3\2\2\2\u0117\u0118\3\2\2\2\u0118R\3\2\2\2\u0119")
        buf.write("\u0117\3\2\2\2\u011a\u011c\t\5\2\2\u011b\u011a\3\2\2\2")
        buf.write("\u011c\u011d\3\2\2\2\u011d\u011b\3\2\2\2\u011d\u011e\3")
        buf.write("\2\2\2\u011e\u011f\3\2\2\2\u011f\u0120\b*\2\2\u0120T\3")
        buf.write("\2\2\2\u0121\u0122\7\62\2\2\u0122\u0123\7z\2\2\u0123\u0125")
        buf.write("\3\2\2\2\u0124\u0126\t\6\2\2\u0125\u0124\3\2\2\2\u0126")
        buf.write("\u0127\3\2\2\2\u0127\u0125\3\2\2\2\u0127\u0128\3\2\2\2")
        buf.write("\u0128\u012f\3\2\2\2\u0129\u012b\t\7\2\2\u012a\u0129\3")
        buf.write("\2\2\2\u012b\u012c\3\2\2\2\u012c\u012a\3\2\2\2\u012c\u012d")
        buf.write("\3\2\2\2\u012d\u012f\3\2\2\2\u012e\u0121\3\2\2\2\u012e")
        buf.write("\u012a\3\2\2\2\u012fV\3\2\2\2\n\2\u0103\u010c\u0117\u011d")
        buf.write("\u0127\u012c\u012e\3\b\2\2")
        return buf.getvalue()


class WiresharkGeneratorLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    T__21 = 22
    UP_ONE = 23
    ABS = 24
    RELATIV = 25
    NE = 26
    GE = 27
    LE = 28
    EQ = 29
    LS = 30
    RS = 31
    SUB = 32
    ADD = 33
    MUL = 34
    AND = 35
    E_BIG = 36
    E_LITTLE = 37
    STRING = 38
    COMMENT = 39
    ID = 40
    WS = 41
    INT = 42

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "';'", "'endian'", "'='", "'dissectorTable'", "'['", "','", 
            "']'", "'protoDetails'", "'{'", "'}'", "'dissectorEntry'", "':'", 
            "'default'", "'enum'", "'typedef'", "'('", "')'", "'void'", 
            "'case'", "'switch'", "'extern'", "'struct'", "'../'", "'/'", 
            "'./'", "'!='", "'>='", "'<='", "'=='", "'<<'", "'>>'", "'-'", 
            "'+'", "'*'", "'&'", "'big'", "'little'" ]

    symbolicNames = [ "<INVALID>",
            "UP_ONE", "ABS", "RELATIV", "NE", "GE", "LE", "EQ", "LS", "RS", 
            "SUB", "ADD", "MUL", "AND", "E_BIG", "E_LITTLE", "STRING", "COMMENT", 
            "ID", "WS", "INT" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "T__14", "T__15", "T__16", "T__17", "T__18", "T__19", 
                  "T__20", "T__21", "UP_ONE", "ABS", "RELATIV", "NE", "GE", 
                  "LE", "EQ", "LS", "RS", "SUB", "ADD", "MUL", "AND", "E_BIG", 
                  "E_LITTLE", "STRING", "COMMENT", "ID", "WS", "INT" ]

    grammarFileName = "WiresharkGenerator.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


