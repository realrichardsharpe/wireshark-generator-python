# The class for the C dissector generator

import sys
from antlr4 import *
from WiresharkGeneratorLexer import WiresharkGeneratorLexer
from WiresharkGeneratorParser import WiresharkGeneratorParser
from WiresharkGeneratorListener import WiresharkGeneratorListener

BIG_ENDIAN = 0
LITTLE_ENDIAN = 1

# TODO move to an approach where each module adds it's own info about these
# types.
proto_types = { 'bit':
                    { 'fieldType': 'FT_BOOLEAN', 'fieldBase': 'size',
                      'luaFieldType': 'boolean', 'luaFieldBase': 'size',
                      'bitLength': '1', 'maxValue': 1},
                'boolean':
                    { 'fieldType': 'FT_BOOLEAN', 'fieldBase': 'size',
                      'luaFieldType': 'boolean', 'luaFieldBase': 'size',
                      'bitLength': '1', 'macValue': 1},
                'uint4':
                    { 'bitLength': 4,
                      'maxValue': 15},
                'byte':
                    { 'bitLength': 8,
                      'maxValue': 255},
                'char':
                    { 'bitLength': 8,
                      'maxValue': 255},
                'string':
                    { 'bitLength': 8,
                      'maxValue': 255},
                'uint8':
                    { 'bitLength': 8,
                      'maxValue': 255},
                'uint16':
                    { 'bitLength': 16,
                      'maxValue': 0xffff},
                'int16':
                    { 'bitLength': 16,
                      'maxValue': 0xffff},
                'uint32':
                    { 'bitLength': 32,
                      'maxValue': 0xffffffff},
                'int32':
                    { 'bitLength': 32,
                      'maxValue': 0xffffffff},
                'uint64':
                    { 'bitLength': 64,
                      'maxValue': 0xffffffffffffffff},
                'int64':
                    { 'bitLength': 64,
                      'maxValue': 0xffffffffffffffff},
                'ether':
                    { 'bitLength': 48,
                      'maxValue': 0xffffffffffff},
                'ether_t':
                    { 'bitLength': 48,
                      'maxValue': 0xffffffffffff},
                'out_t':
                    { 'bitLength': 24,
                      'maxValue': 0xffffff},
                'uint16_le':
                    { 'bitLength': 16,
                      'maxValue': 0xffff},
                'uint16_be':
                    { 'bitLength': 16,
                      'maxValue': 0xffff},
                'uint32_le':
                    { 'bitLength': 32,
                      'maxValue': 0xffffffff},
                'uint32_be':
                    { 'bitLength': 32,
                      'maxValue': 0xffffffff},
                'void':
                    {'bitLength': 0}
              }

class WiresharkASTListener(WiresharkGeneratorListener):

    def __init__(self, verbose_level=0, out_file=sys.stdout, log_file=sys.stderr):
        super().__init__()
        self.semantic_error = False
        self.syntax_error = False
        self.errors = []
        self.endian_type = BIG_ENDIAN
        self.dissector_tables = []
        self.dissector_details = {}
        self.dissector_entry = {}
        self.enums = {}
        self.typedefs = {}
        self.structs = {}
        self.functions = {}
        self.verbose_level = verbose_level
        self.out_file = out_file
        self.log_file = log_file

    def log_it(self, val_str):
        if self.verbose_level > 0:
            self.log_file.write(val_str)
            self.log_file.write('\n')

    def getMaxValueForType(self, type):
        type_info = proto_types.get(type)
        if type_info is not None:
            return type_info['maxValue']
        else:
            return -1

    def getSemanticErrors(self):
        return self.semantic_error

    def getSyntaxErrors(self):
        return self.syntax_error

    def addErrorString(self, error):
        self.errors.append(error)

    # Allow other class instances to set this
    def setSyntaxError(self, error=None):
        self.syntax_error = True
        if error is not None:
            self.addErrorString(error)

    # Add a semantic error and set it to True
    def setSemanticError(self, error=None):
        self.semantic_error = True
        if error is not None:
            self.addErrorString(error)

    # Set an error string for a duplicate definition
    def duplicateDefinition(self, name, line, prev_def):
        error_string = "Duplicate definition of {} on line {}".format(
                                                name, line) + \
                       "\n  Previously defined on line {}".format(
                                                prev_def['line'])
        self.setSemanticError(error_string)
        self.log_it(error_string)

    # get Error Strings
    def getErrorStrings(self):
        return self.errors

    # Enter a protoDecl definition
    def enterProtoDecl(self, ctx:WiresharkGeneratorParser.ProtoDeclContext):
        self.log_it("Entered a protoDecl definition")

    # Exit a protoDecl definition
    def exitProtoDecl(self, ctx:WiresharkGeneratorParser.ProtoDeclContext):
        self.log_it("Exited a protoDecl definition")

    # Enter an endianDecl definition
    def enterEndianDecl(self, ctx:WiresharkGeneratorParser.EndianDeclContext):
        self.log_it("Entered an endianDecl definition")

    # Exit an endianDecl definition
    def exitEndianDecl(self, ctx:WiresharkGeneratorParser.EndianDeclContext):
        if ctx.E_BIG():
            endian = 'Big Endian'
            self.endian_type = BIG_ENDIAN
        else:
            endian = 'Little Endian'
            self.endian_type = LITTLE_ENDIAN
        self.log_it("Exit an endianDecl definition: Endian = {}".format(endian))

    # Enter a dissectorTableDecl.
    def enterDissectorTableDecl(self, ctx:WiresharkGeneratorParser.DissectorTableDeclContext):
        self.log_it("Entered a dissectorTableDecl")

    # Exit a dissectorTableDecl definition
    def exitDissectorTableDecl(self, ctx:WiresharkGeneratorParser.DissectorTableDeclContext):
        self.log_it("Exited a dissectorTableDecl with {} {} {}".format(ctx.STRING(0), ctx.STRING(1), ctx.ID().getText()))
        self.dissector_tables.append({"table":ctx.STRING(0),
                                      "index":ctx.STRING(1),
                                      "func":ctx.ID().getText()})

    # Enter a protoDetailsDecl definition
    def enterProtoDetailsDecl(self, ctx:WiresharkGeneratorParser.ProtoDetailsDeclContext):
        self.log_it("Entering a protoDetailsDecl definition")

    # Exit a protoDetailsDecl definition
    def exitProtoDetailsDecl(self, ctx:WiresharkGeneratorParser.ProtoDetailsDeclContext):
        self.log_it("Exiting a protoDetailsDecl definition {} {} {}".format(
                                    ctx.STRING(0).getText(),
                                    ctx.STRING(1).getText(),
                                    ctx.STRING(2).getText()))
        # Do we need support for multiple of these? Yes: TODO
        self.dissector_details['formalName'] = ctx.STRING(0).getText()
        self.dissector_details['shortName'] = ctx.STRING(1).getText()
        self.dissector_details['abbrev'] = ctx.STRING(2).getText()

    # Get the list of short names
    def getProtocolShortNames(self):
        shortName = self.dissector_details.get('shortName')
        return shortName

    # Enter a dissectorEntryDecl definition
    def enterDissectorEntryDecl(self, ctx:WiresharkGeneratorParser.DissectorEntryDeclContext):
        self.log_it("Entering a dissectorEntryDecl definition")

    # Exit a dissectorEntryDecl definition
    def exitDissectorEntryDecl(self, ctx:WiresharkGeneratorParser.DissectorEntryDeclContext):
        self.log_it("Exiting a dissectorEntryDecl definition {} = {}".format(
                                    ctx.ID(0).getText(),
                                    ctx.ID(1).getText()))
        self.dissector_entry[ctx.ID(0).getText()] = ctx.ID(1).getText()

    # Enter an enumEltDecl definition
    def enterEnumEltDecl(self, ctx:WiresharkGeneratorParser.EnumEltDeclContext):
        self.log_it("Entering an enumEltDecl definition")

    # Exit an enumEltDecl definion
    def exitEnumEltDecl(self, ctx:WiresharkGeneratorParser.EnumEltDeclContext):
        self.log_it("Exiting an enumEltDecl definition")
        # If INT() does not exist, it is a default.
        if ctx.INT() is None:
            self.tmp_enum['default'] = ctx.STRING().getText()
        else:
            enum_elt = {}
            enum_elt['val'] = ctx.INT().getText()
            enum_elt['id'] = ctx.ID().getText()
            if ctx.STRING():
                enum_elt['desc'] = ctx.STRING().getText()
            self.tmp_enum['elts'].append(enum_elt)

    # Enter an enumDecl definition
    def enterEnumDecl(self, ctx:WiresharkGeneratorParser.EnumDeclContext):
        self.log_it("Entering an enumDecl definition")
        self.tmp_enum = {}
        self.tmp_enum['elts'] = []

    # Exit an enumDecl definition
    def exitEnumDecl(self, ctx:WiresharkGeneratorParser.EnumDeclContext):
        self.log_it("Exiting an enumDecl definition for {}".format(ctx.ID(0).getText()))
        enum_name = ctx.ID(0).getText()
        enum_line = ctx.ID(0).getSymbol().line
        enum_type = ctx.ID(1).getText()
        enum_item = self.enums.get(enum_name,)
        if enum_item is not None:
            self.duplicateDefinition(enum_name, enum_line, enum_item)
            # TODO add more error message info
        else:
            self.tmp_enum['line'] = enum_line
            type_info = proto_types.get(enum_type)
            if type_info is None:
                # Check if it is 'bNN'
                if str.lower(enum_type[0:1]) == 'b' and str.isnumeric(enum_type[1:]):
                    self.tmp_enum['bitLength'] = int(enum_type[1:])
                else:
                    self.semantic_error = True
                    self.tmp_enum['bitLength'] = -1
                    self.log_it("Unknown type {} specified on line {}".format(
                                                    enum_name, enum_line))
            else:
                self.tmp_enum['bitLength'] = type_info['bitLength']

            self.log_it("  BitLength = {}".format(self.tmp_enum['bitLength']))
            self.tmp_enum['type'] = enum_type
            self.enums[enum_name] = self.tmp_enum

    # Iterate over the Enums and call the callback
    def iterate_enums(self, callback):
        for enum_name in self.enums.keys():
            callback(enum_name, self.enums[enum_name])

    # Enter a typeDef definition
    def enterTypeDef(self, ctx:WiresharkGeneratorParser.TypeDefContext):
        self.log_it("Entering a typeDef definition")
        self.tmp_typedef = {}

    # Exit a typeDef definition
    def exitTypeDef(self, ctx:WiresharkGeneratorParser.TypeDefContext):
        typedef_name = ctx.ID(1).getText()
        typedef_line = ctx.ID(0).getSymbol().line
        self.log_it("Exiting a typedef definition for {}".format(typedef_name))
        typedef_item = self.typedefs.get(typedef_name,)
        if typedef_item is not None:
            self.duplicateDefinition(typedef_name, typedef_line, typedef_item)
        else:
            self.tmp_typedef['line'] = typedef_line
            self.typedefs[typedef_name] = self.tmp_typedef

    # Enter a param definition
    def enterParam(self, ctx:WiresharkGeneratorParser.ParamContext):
        self.log_it("Entering a param definition")
        self.tmp_param = {}

    # Exit a param definition
    def exitParam(self, ctx:WiresharkGeneratorParser.ParamContext):
        if ctx.INT() is not None:
            self.tmp_param['type'] = 'int'
            self.tmp_param['val'] = ctx.INT().getText()
        elif ctx.STRING() is not None:
            self.tmp_param['type'] = 'string'
            self.tmp_param['val'] = ctx.STRING().getText()
        elif ctx.ID() is not None:
            self.tmp_param['type'] = 'id'
            self.tmp_param['val'] = ctx.ID().getText()
        else: # Must be a fieldpath
            self.tmp_param['type'] = 'fieldpath'
            self.tmp_param['val'] = self.tmp_fieldpath
            self.tmp_fieldpath = None # We have used it!

        self.log_it("Exiting a param definition {}".format(self.tmp_param))
        self.tmp_params.append(self.tmp_param)
        self.tmp_param = None

    # Enter a params definition
    def enterParams(self, ctx:WiresharkGeneratorParser.ParamsContext):
        self.log_it("Entering a params definition")
        self.tmp_params = []

    # Exit a params definition
    def exitParams(self, ctx:WiresharkGeneratorParser.ParamsContext):
        self.log_it("Exiting a params definition params = {}".format(self.tmp_params))
        # Nothing to do here ... our consumer will consume the params

    # Enter a function definition
    def enterFunction(self, ctx:WiresharkGeneratorParser.FunctionContext):
        self.log_it("Entering a function definition")

    # Exit a function definition
    def exitFunction(self, ctx:WiresharkGeneratorParser.FunctionContext):
        self.log_it("Exiting a function definition for {} params = {}".format(ctx.ID().getText(), self.tmp_params))
        self.tmp_function = [ctx.ID().getText(), self.tmp_params]
        self.tmp_params = None

    # Enter a caseDeclDetails definition
    def enterCaseDeclDetails(self, ctx:WiresharkGeneratorParser.CaseDeclDetailsContext):
        self.tmp_case_decl_details = {}
        self.log_it("Entering a caseDeclDetails definition")

    # Exit a caseDeclDetails definition
    def exitCaseDeclDetails(self, ctx:WiresharkGeneratorParser.CaseDeclDetailsContext):
        if ctx.ID() is not None:
            self.tmp_case_decl_details['type'] = ctx.ID().getText()
        elif ctx.structEltDecl() is not None:
            self.log_it("  Some struct elt details")
            self.tmp_case_decl_details
        else:  # We have a void
            self.tmp_case_decl_details['type'] = 'void'

        self.log_it("Exiting a caseDeclDetails definition {}".format(self.tmp_case_decl_details))

    # Enter a caseLabel definition
    def enterCaseLabel(self, ctx:WiresharkGeneratorParser.CaseLabelContext):
        self.log_it("Entering a caseLabel definition")

    # Exit a caseLabel definition
    def exitCaseLabel(self, ctx:WiresharkGeneratorParser.CaseLabelContext):
        if ctx.ID():
            case_label = ctx.ID().getText()
        else:
            case_label = ctx.INT().getText()
        self.log_it("Exiting a caseLabel definition for {}".format(case_label))

    # Enter a caseDecl definition
    def enterCaseDecl(self, ctx:WiresharkGeneratorParser.CaseDeclContext):
        self.log_it("Entering a caseDecl definition")

    # Exit a caseDecl definition
    def exitCaseDecl(self, ctx:WiresharkGeneratorParser.CaseDeclContext):
        if ctx.caseLabel():
            case_label = ctx.caseLabel().getText()
        else:
            case_label = 'DEFAULT'
        tmp_case = {}
        tmp_case['label'] = case_label
        tmp_case['details'] = self.tmp_case_decl_details
        self.tmp_case_decl_details = {}
        self.tmp_cases.append(tmp_case)
        self.log_it("Exiting a caseDecl Definition {} {}".format(case_label, tmp_case))
        tmp_case = None

    # Enter a parse tree produced by WiresharkGeneratorParser#field.
    def enterField(self, ctx:WiresharkGeneratorParser.FieldContext):
        self.log_it("Entering a field definition")

    # Exit a field definition
    def exitField(self, ctx:WiresharkGeneratorParser.FieldContext):
        if ctx.ID():
            field = ctx.ID().getText()
        else:
            field = ctx.STRING().getText()
        self.log_it("Exiting a field definition for {}".format(field))

    # Enter a fieldPath defintion
    def enterFieldPath(self, ctx:WiresharkGeneratorParser.FieldPathContext):
        self.log_it("Entering a fieldPath definition")

    # Exit a fieldPath definition
    def exitFieldPath(self, ctx:WiresharkGeneratorParser.FieldPathContext):
        self.tmp_fieldpath = {'startSym': 'NONE'}
        if ctx.startSym:
            if ctx.ABS():
                startSym = '/'
                self.tmp_fieldpath['startSym'] = 'abs'
            elif ctx.UP_ONE:
                startSym = '../'
                self.tmp_fieldpath['startSym'] = 'up_one'
            elif ctx.RELATIV():
                startSym = './'
                self.tmp_fieldpath['startSym'] = 'relative'
        else:
            startSym = ''
        count = 0
        fieldpath = ''
        for field in ctx.field():
            if count > 0:
                fieldpath = fieldpath + '/'
            fieldpath = fieldpath + field.getText()
            count = count + 1
        self.log_it("Exiting a fieldPath definition {} {}".format(startSym, fieldpath))
        self.tmp_fieldpath['path'] = fieldpath

    # Enter a switchStructEltCtrl definition
    def enterSwitchStructEltCtrl(self, ctx:WiresharkGeneratorParser.SwitchStructEltCtrlContext):
        self.log_it("Entering a switchStructEltCtrl definition")
        self.tmp_switch_ctrl = {}

    # Exit a switchStructEltCtrl definition
    def exitSwitchStructEltCtrl(self, ctx:WiresharkGeneratorParser.SwitchStructEltCtrlContext):
        self.tmp_switch_ctrl['fieldPath'] = self.tmp_fieldpath
        self.tmp_fieldpath = None
        if ctx.op is not None:
            self.log_it("  There is an op {}".format(ctx.op))
            # handle the op
            if ctx.NE():
                self.tmp_switch_ctrl['op'] = '!='
            elif ctx.GE():
                self.tmp_switch_ctrl['op'] = '>='
            elif ctx.LE():
                self.tmp_switch_ctrl['op'] = '<='
            elif ctx.EQ():
                self.tmp_switch_ctrl['op'] = '=='
            elif ctx.LS():
                self.tmp_switch_ctrl['op'] = '<<'
            elif ctx.RS():
                self.tmp_switch_ctrl['op'] = '>>'
            elif ctx.ADD():
                self.tmp_switch_ctrl['op'] = '+'
            elif ctx.SUB():
                self.tmp_switch_ctrl['op'] = '-'
            elif ctx.AND():
                self.tmp_switch_ctrl['op'] = '&'
            if ctx.INT():
                self.tmp_switch_ctrl['rhs'] = ctx.INT().getText()
            else:
                self.tmp_switch_ctrl['rhs'] = ctx.ID().getText()

        self.log_it("Exiting a switchStructEltCtrl definition = {}".format(self.tmp_switch_ctrl))

    # Enter a switchDecl definition
    def enterSwitchDecl(self, ctx:WiresharkGeneratorParser.SwitchDeclContext):
        self.log_it("Entering a switchDecl definition")
        self.tmp_switch_decl = {}
        self.tmp_cases = []

    # Exit a switchDecl definition
    def exitSwitchDecl(self, ctx:WiresharkGeneratorParser.SwitchDeclContext):
        self.tmp_switch_decl['control'] = self.tmp_switch_ctrl
        self.tmp_switch_decl['cases'] = self.tmp_cases
        self.tmp_switch_ctrl = None
        self.tmp_cases = None
        self.log_it("Exiting a switchDecl definition {}".format(self.tmp_switch_decl))
        self.tmp_struct['fields'].append(self.tmp_switch_decl)

    # Enter an externEltDecl definition
    def enterExternEltDecl(self, ctx:WiresharkGeneratorParser.ExternEltDeclContext):
        self.log_it("Entering an externEltDecl definition")

    # Exit an externEltDecl definition
    # This one needs more thought because it implies that a series of bytes
    # Can be handled and thus we do not know how many bytes there might be.
    def exitExternEltDecl(self, ctx:WiresharkGeneratorParser.ExternEltDeclContext):
        self.tmp_extern_elt = {}
        self.tmp_extern_elt['table'] = ctx.STRING(0).getText()
        self.tmp_extern_elt['type'] = ctx.ID(0).getText()
        if ctx.ID(1) is not None:
            self.tmp_extern_elt['field'] = ctx.ID(1).getText()
        else:
            self.tmp_extern_elt['field'] = ctx.STRING().getText()
        self.tmp_struct['fields'].append(self.tmp_extern_elt)
        self.log_it("Exiting an externEltDecl definition {}".format(self.tmp_extern_elt))

    # Enter a localEltDeclCont definition
    def enterLocalEltDeclCont(self, ctx:WiresharkGeneratorParser.LocalEltDeclContContext):
        self.log_it("Entering a localEltDeclCont definition")

    # Exit a localEltDeclCont definition
    def exitLocalEltDeclCont(self, ctx:WiresharkGeneratorParser.LocalEltDeclContContext):
        local_elt_decl = {}
        local_elt_decl['start'] = ctx.INT(0).getText()
        if len(ctx.INT()) > 1:
            local_elt_decl['end'] = ctx.INT(1).getText()
        else:
            local_elt_decl['end'] = ctx.INT(0).getText()
        local_elt_decl['type'] = ctx.ID(0).getText()
        if len(ctx.ID()) > 1:
            local_elt_decl['field'] = ctx.ID(1).getText()
        else:
            local_elt_decl['field'] = ctx.STRING().getText()
        self.tmp_localeltdeclcont.append(local_elt_decl)
        self.log_it("Exiting a localEltDeclCont definition {}".format(self.tmp_localeltdeclcont))

    # Enter a localEltDecl definition
    def enterLocalEltDecl(self, ctx:WiresharkGeneratorParser.LocalEltDeclContext):
        self.log_it("Entering localEltDecl definition")
        self.tmp_localeltdecl = {}
        self.tmp_localeltdeclcont = []

    # Exit a localEltDecl definition
    def exitLocalEltDecl(self, ctx:WiresharkGeneratorParser.LocalEltDeclContext):
        self.tmp_localeltdecl['type'] = ctx.ID(0).getText()
        if len(ctx.ID()) > 1: # It's a field name
            self.tmp_localeltdecl['field'] = ctx.ID(1).getText()
        elif ctx.STRING() is not None:
            self.tmp_localeltdecl['field'] = ctx.STRING().getText()
        else: # Must be one or more continuations
            self.tmp_localeltdecl['cont'] = self.tmp_localeltdeclcont
            self.tmp_localeltdeclcont = None

        self.log_it("Exiting localEltDecl definition {}".format(self.tmp_localeltdecl))
        self.tmp_struct['fields'].append(self.tmp_localeltdecl)
        self.tmp_localeltdecl = None
        self.log_it("  self.tmp_struct = {}".format(self.tmp_struct))

    # Enter a arrayEltDecl definition
    def enterArrayEltDecl(self, ctx:WiresharkGeneratorParser.ArrayEltDeclContext):
        self.log_it("Entering arrayEltDecl definition")

    # Exit an arrayEltDecl definition
    def exitArrayEltDecl(self, ctx:WiresharkGeneratorParser.ArrayEltDeclContext):
        if len(ctx.ID()) > 1:
            array_name = ctx.ID(1).getText()
        else:
            array_name = ctx.STRING().getText()
        self.log_it("Name is a string {}".format(array_name))

        array_type = ctx.ID(0).getText()

        array_elt = {}
        array_elt['name'] = array_name
        array_elt['type'] = array_type
        # Deal with the length ... which can be either an int or a
        # switchStructEltCtrl
        if ctx.INT() is not None:
            array_elt['size'] = ctx.INT().getText()    
        else:
            array_elt['size_field'] = self.tmp_fieldpath
            self.log_it("Fieldpath is {}".format(self.tmp_fieldpath))
            self.tmp_fieldpath = None

        self.log_it("Exiting arrayEltDecl definition {} of type {}".format(
                                                    array_name, array_type))
        self.tmp_struct['fields'].append(array_elt)

    # Enter a structEltDecl definition
    def enterStructEltDecl(self, ctx:WiresharkGeneratorParser.StructEltDeclContext):
        self.log_it("Entering structEltDecl definition")

    # Exit a structEltDecl definition
    # This has to keep a self.tmp_struct_elt_decl for others to take
    def exitStructEltDecl(self, ctx:WiresharkGeneratorParser.StructEltDeclContext):
        if ctx.externEltDecl() is not None:
            self.log_it('externEltDecl: tmp_struct = {}'.format(self.tmp_struct))
            self.tmp_structEltDecls.append(self.tmp_externEltDecl)
        elif ctx.function() is not None:
            self.log_it('function: tmp_struct = {}'.format(self.tmp_struct))
        elif ctx.localEltDecl() is not None:
            self.log_it('localEltDecl: tmp_struct = {}'.format(self.tmp_struct))
            self.tmp_localeltdecl = None
        elif ctx.arrayEltDecl() is not None:
             self.log_it('arrayEltDecl: tmp_struct = {}'.format(self.tmp_struct))
             self.tmp_array_elt = None
        else:
             self.log_it('switchDecl: tmp_struct = {}'.format(self.tmp_struct))
        self.log_it("Exiting structEltDecl definition {}".format(self.tmp_struct))
        self.log_it("self.tmp_struct = {}".format(self.tmp_struct))

    # Enter a structDecl definition
    def enterStructDecl(self, ctx:WiresharkGeneratorParser.StructDeclContext):
        self.log_it("Entering structDecl definition")
        # These things need to be in order, so use a list
        self.tmp_struct = {}
        self.tmp_struct['fields'] = []
        self.tmp_structEltDecls = []

    # Exit a structDecl definition
    def exitStructDecl(self, ctx:WiresharkGeneratorParser.StructDeclContext):
        struct_name = ctx.ID().getText()
        struct_line = ctx.ID().getSymbol().line
        struct_item = self.structs.get(struct_name,)
        if struct_item is not None:
            self.semantic_error = True
            self.duplicateDefinition(struct_name, struct_line, struct_item)
        else:
            self.tmp_struct['line'] = struct_line
        self.tmp_struct['fields'] = self.tmp_structEltDecls
        self.structs[struct_name] = self.tmp_struct
        self.log_it("Exiting structDecl definition for {}".format(self.tmp_struct))
        self.tmp_struct = None

    # iterate_structs: Iterate through the structs and call a callback for 
    # each one.
    def iterate_structs(self, callback):
        for struct_name in self.structs.keys():
            callback(struct_name, self.structs[struct_name])

    # Enter a protocol definition
    def enterProtocol(self, ctx:WiresharkGeneratorParser.ProtocolContext):
        self.log_it("Entered a protocol definition")

    # Exit a protocol definition
    def exitProtocol(self, ctx:WiresharkGeneratorParser.ProtocolContext):
        self.log_it("Exited a protocol definition")

