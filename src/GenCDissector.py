# Generate a dissector
# We need to build each section ...
# 
# Generate boilerplate
#
# Generate HF Fields for protocols
#
# Generate the HF Declarations need to iterate the structs and callback

import sys

# Get an int val but check if it is a hex num, octal, binary or decimal
# TODO move to its own file.
def getIntVal(val):
    if type(val) is int:
        return val
    if type(val) is str:
        if val[0:2] == '0x':
            return int(val, base=16)
        else: # Assume decimal
            return int(val)
    return None

class GenCDissector:

    def __init__(self, log_it, parent_ast, verbose_level=0, outfile=sys.stdout):
        self.log_it = log_it
        self.ast = parent_ast
        self.verbose_level = verbose_level
        self.outfile = outfile
        self.etts = []
        self.hfs = {}

    def code(self, code_line):
        self.outfile.write(code_line)
        self.outfile.write('\n')

    # Generate the boilerplate stuff
    def generateInitialBoilerPlate(self):
        self.code('#include "config.h"')
        self.code('#include <epan/packet.h>')
        self.code('#include <epan/expert.h>')
        self.code('')

    # Generate the protocol header field stuff?
    def generateProtocolsHeaderFields(self):
        shortNames = self.ast.getProtocolShortNames()
        if shortNames is not None:
            for shortname in shortNames:
                str_shortname = shortname.strip('"')
                self.code('static int proto_{} = -1;'.format(str_shortname))
            self.code('')

    # Generate a single header field
    def generateHeaderField(self, struct_name, struct):
        self.code("//Generating a header field for {}".format(struct_name))
        self.etts.append(struct_name)

    # Iterate through the structs in the protocol and generate HF entries.
    def generateHeaderFields(self):
        self.ast.iterate_structs(self.generateHeaderField)
        self.code('')

    # Generate Expert Infos
    def generateExpertInfos(self):
        pass

    # Generate ETT Declarations
    def generateETTDecls(self):
        for ett in self.etts:
            self.code('static gint ett_{} = =1;'.format(ett))
        self.code('')

    # Generate the code for a boolean enum
    def generateEnumBooleanCode(self, enum_name, enum):
        self.code('static const true_false_string {}_tfs = {{'.format(
                                                                    enum_name))
        # TODO: If desc is not there, use the ID
        if enum['elts'][1].get('desc') is not None:
            self.code('  {},'.format(enum['elts'][1]['desc']))
        else:
            self.code('  {},'.format(enum['elts'][1]['id']))
        if enum['elts'][0].get('desc') is not None:
            self.code('  {}'.format(enum['elts'][0]['desc']))
        else:
            self.code('  {}'.format(enum['elts'][0]['id']))
        self.code('};')
        self.code('')

    # Generate the code for a non boolean enum
    def generateEnumNonBooleanCode(self, enum_name, enum):
        # generate the range string
        prev_val = 0
        if enum.get('default') is not None:
            default = enum['default']
        else:
            default = 'Reserved'
        self.code('static const range_string {}_rvals[] = {{'.format(enum_name))
        for elt in enum['elts']:
            current = getIntVal(elt['val'])
            if current > prev_val:
                self.code('  {{ {}, {}, {}, }}'.format(prev_val, current - 1,
                                                       default))
            self.code('  {{ {}, {}, {} }},'.format(elt['val'], elt['val'],
                                                   elt['desc']))
            prev_val = current + 1;
        max_val = self.ast.getMaxValueForType(enum['type'])
        if prev_val < max_val:
            self.code('  {{ {}. {}, {} }},'.format(prev_val, max_val, default))
        self.code('};')
        self.code('')

    # Generate the code required for a single enum
    def generateEnumCode(self, enum_name, enum):
        self.code('//Generating code for enum {}'.format(enum_name))
        self.code('enum {} {{'.format(enum_name))
        for elt in enum['elts']:
            self.code('  {} = {};'.format(elt['id'], elt['val']))
        self.code('};')
        self.code('')
        if enum['type'] in ['bit', 'boolean']:
            self.code('//We have a bit/boolean')
            self.generateEnumBooleanCode(enum_name, enum)
        else:
            self.code('//We have a {}'.format(enum['type']))
            self.generateEnumNonBooleanCode(enum_name, enum)

    # Generate Enum Decls
    def generateEnumDecls(self):
        self.ast.iterate_enums(self.generateEnumCode)

    # Generate forward declarations
    def generateForwardDecls(self):
        pass

    # Generate Dissectors
    def generateDissectors(self):
        pass

    # Generate Registrations
    def generateRegistrations(self):
        pass

    # Generate trailer
    def generateTrailer(self):
        pass

    def generateCode(self):
        self.generateInitialBoilerPlate()
        self.generateProtocolsHeaderFields()
        self.generateHeaderFields()
        self.generateExpertInfos()
        self.generateETTDecls()
        self.generateEnumDecls()
        self.generateForwardDecls()
        self.generateDissectors()
        self.generateRegistrations()
        self.generateTrailer()
