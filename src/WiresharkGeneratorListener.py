# Generated from ../grammar/WiresharkGenerator.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .WiresharkGeneratorParser import WiresharkGeneratorParser
else:
    from WiresharkGeneratorParser import WiresharkGeneratorParser

# This class defines a complete listener for a parse tree produced by WiresharkGeneratorParser.
class WiresharkGeneratorListener(ParseTreeListener):

    # Enter a parse tree produced by WiresharkGeneratorParser#protocol.
    def enterProtocol(self, ctx:WiresharkGeneratorParser.ProtocolContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#protocol.
    def exitProtocol(self, ctx:WiresharkGeneratorParser.ProtocolContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#protoDecl.
    def enterProtoDecl(self, ctx:WiresharkGeneratorParser.ProtoDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#protoDecl.
    def exitProtoDecl(self, ctx:WiresharkGeneratorParser.ProtoDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#endianDecl.
    def enterEndianDecl(self, ctx:WiresharkGeneratorParser.EndianDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#endianDecl.
    def exitEndianDecl(self, ctx:WiresharkGeneratorParser.EndianDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#dissectorTableDecl.
    def enterDissectorTableDecl(self, ctx:WiresharkGeneratorParser.DissectorTableDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#dissectorTableDecl.
    def exitDissectorTableDecl(self, ctx:WiresharkGeneratorParser.DissectorTableDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#protoDetailsDecl.
    def enterProtoDetailsDecl(self, ctx:WiresharkGeneratorParser.ProtoDetailsDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#protoDetailsDecl.
    def exitProtoDetailsDecl(self, ctx:WiresharkGeneratorParser.ProtoDetailsDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#dissectorEntryDecl.
    def enterDissectorEntryDecl(self, ctx:WiresharkGeneratorParser.DissectorEntryDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#dissectorEntryDecl.
    def exitDissectorEntryDecl(self, ctx:WiresharkGeneratorParser.DissectorEntryDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#enumEltDecl.
    def enterEnumEltDecl(self, ctx:WiresharkGeneratorParser.EnumEltDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#enumEltDecl.
    def exitEnumEltDecl(self, ctx:WiresharkGeneratorParser.EnumEltDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#enumDecl.
    def enterEnumDecl(self, ctx:WiresharkGeneratorParser.EnumDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#enumDecl.
    def exitEnumDecl(self, ctx:WiresharkGeneratorParser.EnumDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#typeDef.
    def enterTypeDef(self, ctx:WiresharkGeneratorParser.TypeDefContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#typeDef.
    def exitTypeDef(self, ctx:WiresharkGeneratorParser.TypeDefContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#param.
    def enterParam(self, ctx:WiresharkGeneratorParser.ParamContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#param.
    def exitParam(self, ctx:WiresharkGeneratorParser.ParamContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#params.
    def enterParams(self, ctx:WiresharkGeneratorParser.ParamsContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#params.
    def exitParams(self, ctx:WiresharkGeneratorParser.ParamsContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#function.
    def enterFunction(self, ctx:WiresharkGeneratorParser.FunctionContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#function.
    def exitFunction(self, ctx:WiresharkGeneratorParser.FunctionContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#caseDeclDetails.
    def enterCaseDeclDetails(self, ctx:WiresharkGeneratorParser.CaseDeclDetailsContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#caseDeclDetails.
    def exitCaseDeclDetails(self, ctx:WiresharkGeneratorParser.CaseDeclDetailsContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#caseLabel.
    def enterCaseLabel(self, ctx:WiresharkGeneratorParser.CaseLabelContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#caseLabel.
    def exitCaseLabel(self, ctx:WiresharkGeneratorParser.CaseLabelContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#caseDecl.
    def enterCaseDecl(self, ctx:WiresharkGeneratorParser.CaseDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#caseDecl.
    def exitCaseDecl(self, ctx:WiresharkGeneratorParser.CaseDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#field.
    def enterField(self, ctx:WiresharkGeneratorParser.FieldContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#field.
    def exitField(self, ctx:WiresharkGeneratorParser.FieldContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#fieldPath.
    def enterFieldPath(self, ctx:WiresharkGeneratorParser.FieldPathContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#fieldPath.
    def exitFieldPath(self, ctx:WiresharkGeneratorParser.FieldPathContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#switchStructEltCtrl.
    def enterSwitchStructEltCtrl(self, ctx:WiresharkGeneratorParser.SwitchStructEltCtrlContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#switchStructEltCtrl.
    def exitSwitchStructEltCtrl(self, ctx:WiresharkGeneratorParser.SwitchStructEltCtrlContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#switchDecl.
    def enterSwitchDecl(self, ctx:WiresharkGeneratorParser.SwitchDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#switchDecl.
    def exitSwitchDecl(self, ctx:WiresharkGeneratorParser.SwitchDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#externEltDecl.
    def enterExternEltDecl(self, ctx:WiresharkGeneratorParser.ExternEltDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#externEltDecl.
    def exitExternEltDecl(self, ctx:WiresharkGeneratorParser.ExternEltDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#localEltDeclCont.
    def enterLocalEltDeclCont(self, ctx:WiresharkGeneratorParser.LocalEltDeclContContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#localEltDeclCont.
    def exitLocalEltDeclCont(self, ctx:WiresharkGeneratorParser.LocalEltDeclContContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#localEltDecl.
    def enterLocalEltDecl(self, ctx:WiresharkGeneratorParser.LocalEltDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#localEltDecl.
    def exitLocalEltDecl(self, ctx:WiresharkGeneratorParser.LocalEltDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#arrayEltDecl.
    def enterArrayEltDecl(self, ctx:WiresharkGeneratorParser.ArrayEltDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#arrayEltDecl.
    def exitArrayEltDecl(self, ctx:WiresharkGeneratorParser.ArrayEltDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#structEltDecl.
    def enterStructEltDecl(self, ctx:WiresharkGeneratorParser.StructEltDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#structEltDecl.
    def exitStructEltDecl(self, ctx:WiresharkGeneratorParser.StructEltDeclContext):
        pass


    # Enter a parse tree produced by WiresharkGeneratorParser#structDecl.
    def enterStructDecl(self, ctx:WiresharkGeneratorParser.StructDeclContext):
        pass

    # Exit a parse tree produced by WiresharkGeneratorParser#structDecl.
    def exitStructDecl(self, ctx:WiresharkGeneratorParser.StructDeclContext):
        pass



del WiresharkGeneratorParser