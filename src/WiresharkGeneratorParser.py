# Generated from ../grammar/WiresharkGenerator.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3,")
        buf.write("\u011c\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\3\2\6\2\66\n\2\r\2\16\2\67\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\5\3O\n\3\3\4\3\4\3\4\3\4\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6")
        buf.write("\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\5")
        buf.write("\br\n\b\3\b\3\b\3\b\5\bw\n\b\3\t\3\t\3\t\3\t\5\t}\n\t")
        buf.write("\3\t\3\t\3\t\3\t\7\t\u0083\n\t\f\t\16\t\u0086\13\t\3\t")
        buf.write("\5\t\u0089\n\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u0093")
        buf.write("\n\n\3\13\3\13\3\13\3\13\5\13\u0099\n\13\3\f\3\f\3\f\7")
        buf.write("\f\u009e\n\f\f\f\16\f\u00a1\13\f\3\r\3\r\3\r\3\r\3\r\3")
        buf.write("\16\3\16\3\16\3\16\3\16\3\16\6\16\u00ae\n\16\r\16\16\16")
        buf.write("\u00af\3\16\3\16\3\16\5\16\u00b5\n\16\3\17\3\17\3\20\3")
        buf.write("\20\3\20\5\20\u00bc\n\20\3\20\3\20\3\20\3\20\3\21\3\21")
        buf.write("\3\22\5\22\u00c5\n\22\3\22\3\22\3\22\7\22\u00ca\n\22\f")
        buf.write("\22\16\22\u00cd\13\22\3\23\3\23\3\23\3\23\3\23\5\23\u00d4")
        buf.write("\n\23\3\24\3\24\3\24\3\24\3\24\3\24\6\24\u00dc\n\24\r")
        buf.write("\24\16\24\u00dd\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25")
        buf.write("\3\25\3\26\3\26\3\26\3\26\5\26\u00ed\n\26\3\26\3\26\3")
        buf.write("\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\7\27\u00f9\n\27")
        buf.write("\f\27\16\27\u00fc\13\27\5\27\u00fe\n\27\3\30\3\30\3\30")
        buf.write("\3\30\3\30\5\30\u0105\n\30\3\30\3\30\3\31\3\31\3\31\3")
        buf.write("\31\3\31\5\31\u010e\n\31\3\32\3\32\3\32\3\32\3\32\3\32")
        buf.write("\6\32\u0116\n\32\r\32\16\32\u0117\3\32\3\32\3\32\2\2\33")
        buf.write("\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62")
        buf.write("\2\7\3\2&\'\4\2**,,\4\2((**\3\2\31\33\4\2\34#%%\2\u0125")
        buf.write("\2\65\3\2\2\2\4N\3\2\2\2\6P\3\2\2\2\bT\3\2\2\2\n]\3\2")
        buf.write("\2\2\fg\3\2\2\2\16v\3\2\2\2\20x\3\2\2\2\22\u008c\3\2\2")
        buf.write("\2\24\u0098\3\2\2\2\26\u009a\3\2\2\2\30\u00a2\3\2\2\2")
        buf.write("\32\u00b4\3\2\2\2\34\u00b6\3\2\2\2\36\u00bb\3\2\2\2 \u00c1")
        buf.write("\3\2\2\2\"\u00c4\3\2\2\2$\u00d3\3\2\2\2&\u00d5\3\2\2\2")
        buf.write("(\u00e1\3\2\2\2*\u00e8\3\2\2\2,\u00fd\3\2\2\2.\u00ff\3")
        buf.write("\2\2\2\60\u010d\3\2\2\2\62\u010f\3\2\2\2\64\66\5\4\3\2")
        buf.write("\65\64\3\2\2\2\66\67\3\2\2\2\67\65\3\2\2\2\678\3\2\2\2")
        buf.write("8\3\3\2\2\29:\5\b\5\2:;\7\3\2\2;O\3\2\2\2<=\5\6\4\2=>")
        buf.write("\7\3\2\2>O\3\2\2\2?@\5\n\6\2@A\7\3\2\2AO\3\2\2\2BC\5\f")
        buf.write("\7\2CD\7\3\2\2DO\3\2\2\2EF\5\20\t\2FG\7\3\2\2GO\3\2\2")
        buf.write("\2HI\5\62\32\2IJ\7\3\2\2JO\3\2\2\2KL\5\22\n\2LM\7\3\2")
        buf.write("\2MO\3\2\2\2N9\3\2\2\2N<\3\2\2\2N?\3\2\2\2NB\3\2\2\2N")
        buf.write("E\3\2\2\2NH\3\2\2\2NK\3\2\2\2O\5\3\2\2\2PQ\7\4\2\2QR\7")
        buf.write("\5\2\2RS\t\2\2\2S\7\3\2\2\2TU\7\6\2\2UV\7\7\2\2VW\7(\2")
        buf.write("\2WX\7\b\2\2XY\7(\2\2YZ\7\t\2\2Z[\7\5\2\2[\\\7*\2\2\\")
        buf.write("\t\3\2\2\2]^\7\n\2\2^_\7\5\2\2_`\7\13\2\2`a\7(\2\2ab\7")
        buf.write("\b\2\2bc\7(\2\2cd\7\b\2\2de\7(\2\2ef\7\f\2\2f\13\3\2\2")
        buf.write("\2gh\7\r\2\2hi\7*\2\2ij\7\5\2\2jk\7*\2\2k\r\3\2\2\2lm")
        buf.write("\7,\2\2mn\7\5\2\2nq\7*\2\2op\7\16\2\2pr\7(\2\2qo\3\2\2")
        buf.write("\2qr\3\2\2\2rw\3\2\2\2st\7\17\2\2tu\7\5\2\2uw\7(\2\2v")
        buf.write("l\3\2\2\2vs\3\2\2\2w\17\3\2\2\2xy\7\20\2\2y|\7*\2\2z{")
        buf.write("\7\16\2\2{}\7*\2\2|z\3\2\2\2|}\3\2\2\2}~\3\2\2\2~\177")
        buf.write("\7\13\2\2\177\u0084\5\16\b\2\u0080\u0081\7\b\2\2\u0081")
        buf.write("\u0083\5\16\b\2\u0082\u0080\3\2\2\2\u0083\u0086\3\2\2")
        buf.write("\2\u0084\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085\u0088")
        buf.write("\3\2\2\2\u0086\u0084\3\2\2\2\u0087\u0089\7\b\2\2\u0088")
        buf.write("\u0087\3\2\2\2\u0088\u0089\3\2\2\2\u0089\u008a\3\2\2\2")
        buf.write("\u008a\u008b\7\f\2\2\u008b\21\3\2\2\2\u008c\u008d\7\21")
        buf.write("\2\2\u008d\u008e\7*\2\2\u008e\u0092\7*\2\2\u008f\u0090")
        buf.write("\7\7\2\2\u0090\u0091\7,\2\2\u0091\u0093\7\t\2\2\u0092")
        buf.write("\u008f\3\2\2\2\u0092\u0093\3\2\2\2\u0093\23\3\2\2\2\u0094")
        buf.write("\u0099\7,\2\2\u0095\u0099\7(\2\2\u0096\u0099\7*\2\2\u0097")
        buf.write("\u0099\5\"\22\2\u0098\u0094\3\2\2\2\u0098\u0095\3\2\2")
        buf.write("\2\u0098\u0096\3\2\2\2\u0098\u0097\3\2\2\2\u0099\25\3")
        buf.write("\2\2\2\u009a\u009f\5\24\13\2\u009b\u009c\7\b\2\2\u009c")
        buf.write("\u009e\5\24\13\2\u009d\u009b\3\2\2\2\u009e\u00a1\3\2\2")
        buf.write("\2\u009f\u009d\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\27\3")
        buf.write("\2\2\2\u00a1\u009f\3\2\2\2\u00a2\u00a3\7*\2\2\u00a3\u00a4")
        buf.write("\7\22\2\2\u00a4\u00a5\5\26\f\2\u00a5\u00a6\7\23\2\2\u00a6")
        buf.write("\31\3\2\2\2\u00a7\u00b5\7*\2\2\u00a8\u00b5\5\60\31\2\u00a9")
        buf.write("\u00ad\7\13\2\2\u00aa\u00ab\5\60\31\2\u00ab\u00ac\7\3")
        buf.write("\2\2\u00ac\u00ae\3\2\2\2\u00ad\u00aa\3\2\2\2\u00ae\u00af")
        buf.write("\3\2\2\2\u00af\u00ad\3\2\2\2\u00af\u00b0\3\2\2\2\u00b0")
        buf.write("\u00b1\3\2\2\2\u00b1\u00b2\7\f\2\2\u00b2\u00b5\3\2\2\2")
        buf.write("\u00b3\u00b5\7\24\2\2\u00b4\u00a7\3\2\2\2\u00b4\u00a8")
        buf.write("\3\2\2\2\u00b4\u00a9\3\2\2\2\u00b4\u00b3\3\2\2\2\u00b5")
        buf.write("\33\3\2\2\2\u00b6\u00b7\t\3\2\2\u00b7\35\3\2\2\2\u00b8")
        buf.write("\u00b9\7\25\2\2\u00b9\u00bc\5\34\17\2\u00ba\u00bc\7\17")
        buf.write("\2\2\u00bb\u00b8\3\2\2\2\u00bb\u00ba\3\2\2\2\u00bc\u00bd")
        buf.write("\3\2\2\2\u00bd\u00be\7\16\2\2\u00be\u00bf\5\32\16\2\u00bf")
        buf.write("\u00c0\7\3\2\2\u00c0\37\3\2\2\2\u00c1\u00c2\t\4\2\2\u00c2")
        buf.write("!\3\2\2\2\u00c3\u00c5\t\5\2\2\u00c4\u00c3\3\2\2\2\u00c4")
        buf.write("\u00c5\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c6\u00cb\5 \21\2")
        buf.write("\u00c7\u00c8\7\32\2\2\u00c8\u00ca\5 \21\2\u00c9\u00c7")
        buf.write("\3\2\2\2\u00ca\u00cd\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cb")
        buf.write("\u00cc\3\2\2\2\u00cc#\3\2\2\2\u00cd\u00cb\3\2\2\2\u00ce")
        buf.write("\u00d4\5\"\22\2\u00cf\u00d0\5\"\22\2\u00d0\u00d1\t\6\2")
        buf.write("\2\u00d1\u00d2\t\3\2\2\u00d2\u00d4\3\2\2\2\u00d3\u00ce")
        buf.write("\3\2\2\2\u00d3\u00cf\3\2\2\2\u00d4%\3\2\2\2\u00d5\u00d6")
        buf.write("\7\26\2\2\u00d6\u00d7\7\22\2\2\u00d7\u00d8\5$\23\2\u00d8")
        buf.write("\u00d9\7\23\2\2\u00d9\u00db\7\13\2\2\u00da\u00dc\5\36")
        buf.write("\20\2\u00db\u00da\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00db")
        buf.write("\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00df\3\2\2\2\u00df")
        buf.write("\u00e0\7\f\2\2\u00e0\'\3\2\2\2\u00e1\u00e2\7\27\2\2\u00e2")
        buf.write("\u00e3\7\7\2\2\u00e3\u00e4\7(\2\2\u00e4\u00e5\7\t\2\2")
        buf.write("\u00e5\u00e6\7*\2\2\u00e6\u00e7\t\4\2\2\u00e7)\3\2\2\2")
        buf.write("\u00e8\u00e9\7\16\2\2\u00e9\u00ec\7,\2\2\u00ea\u00eb\7")
        buf.write("\"\2\2\u00eb\u00ed\7,\2\2\u00ec\u00ea\3\2\2\2\u00ec\u00ed")
        buf.write("\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00ef\7\16\2\2\u00ef")
        buf.write("\u00f0\7*\2\2\u00f0\u00f1\t\4\2\2\u00f1+\3\2\2\2\u00f2")
        buf.write("\u00f3\7*\2\2\u00f3\u00fe\t\4\2\2\u00f4\u00f5\7*\2\2\u00f5")
        buf.write("\u00fa\5*\26\2\u00f6\u00f7\7\b\2\2\u00f7\u00f9\5*\26\2")
        buf.write("\u00f8\u00f6\3\2\2\2\u00f9\u00fc\3\2\2\2\u00fa\u00f8\3")
        buf.write("\2\2\2\u00fa\u00fb\3\2\2\2\u00fb\u00fe\3\2\2\2\u00fc\u00fa")
        buf.write("\3\2\2\2\u00fd\u00f2\3\2\2\2\u00fd\u00f4\3\2\2\2\u00fe")
        buf.write("-\3\2\2\2\u00ff\u0100\7*\2\2\u0100\u0101\t\4\2\2\u0101")
        buf.write("\u0104\7\7\2\2\u0102\u0105\7,\2\2\u0103\u0105\5$\23\2")
        buf.write("\u0104\u0102\3\2\2\2\u0104\u0103\3\2\2\2\u0105\u0106\3")
        buf.write("\2\2\2\u0106\u0107\7\t\2\2\u0107/\3\2\2\2\u0108\u010e")
        buf.write("\5(\25\2\u0109\u010e\5\30\r\2\u010a\u010e\5,\27\2\u010b")
        buf.write("\u010e\5.\30\2\u010c\u010e\5&\24\2\u010d\u0108\3\2\2\2")
        buf.write("\u010d\u0109\3\2\2\2\u010d\u010a\3\2\2\2\u010d\u010b\3")
        buf.write("\2\2\2\u010d\u010c\3\2\2\2\u010e\61\3\2\2\2\u010f\u0110")
        buf.write("\7\30\2\2\u0110\u0111\7*\2\2\u0111\u0115\7\13\2\2\u0112")
        buf.write("\u0113\5\60\31\2\u0113\u0114\7\3\2\2\u0114\u0116\3\2\2")
        buf.write("\2\u0115\u0112\3\2\2\2\u0116\u0117\3\2\2\2\u0117\u0115")
        buf.write("\3\2\2\2\u0117\u0118\3\2\2\2\u0118\u0119\3\2\2\2\u0119")
        buf.write("\u011a\7\f\2\2\u011a\63\3\2\2\2\31\67Nqv|\u0084\u0088")
        buf.write("\u0092\u0098\u009f\u00af\u00b4\u00bb\u00c4\u00cb\u00d3")
        buf.write("\u00dd\u00ec\u00fa\u00fd\u0104\u010d\u0117")
        return buf.getvalue()


class WiresharkGeneratorParser ( Parser ):

    grammarFileName = "WiresharkGenerator.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "';'", "'endian'", "'='", "'dissectorTable'", 
                     "'['", "','", "']'", "'protoDetails'", "'{'", "'}'", 
                     "'dissectorEntry'", "':'", "'default'", "'enum'", "'typedef'", 
                     "'('", "')'", "'void'", "'case'", "'switch'", "'extern'", 
                     "'struct'", "'../'", "'/'", "'./'", "'!='", "'>='", 
                     "'<='", "'=='", "'<<'", "'>>'", "'-'", "'+'", "'*'", 
                     "'&'", "'big'", "'little'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "UP_ONE", "ABS", 
                      "RELATIV", "NE", "GE", "LE", "EQ", "LS", "RS", "SUB", 
                      "ADD", "MUL", "AND", "E_BIG", "E_LITTLE", "STRING", 
                      "COMMENT", "ID", "WS", "INT" ]

    RULE_protocol = 0
    RULE_protoDecl = 1
    RULE_endianDecl = 2
    RULE_dissectorTableDecl = 3
    RULE_protoDetailsDecl = 4
    RULE_dissectorEntryDecl = 5
    RULE_enumEltDecl = 6
    RULE_enumDecl = 7
    RULE_typeDef = 8
    RULE_param = 9
    RULE_params = 10
    RULE_function = 11
    RULE_caseDeclDetails = 12
    RULE_caseLabel = 13
    RULE_caseDecl = 14
    RULE_field = 15
    RULE_fieldPath = 16
    RULE_switchStructEltCtrl = 17
    RULE_switchDecl = 18
    RULE_externEltDecl = 19
    RULE_localEltDeclCont = 20
    RULE_localEltDecl = 21
    RULE_arrayEltDecl = 22
    RULE_structEltDecl = 23
    RULE_structDecl = 24

    ruleNames =  [ "protocol", "protoDecl", "endianDecl", "dissectorTableDecl", 
                   "protoDetailsDecl", "dissectorEntryDecl", "enumEltDecl", 
                   "enumDecl", "typeDef", "param", "params", "function", 
                   "caseDeclDetails", "caseLabel", "caseDecl", "field", 
                   "fieldPath", "switchStructEltCtrl", "switchDecl", "externEltDecl", 
                   "localEltDeclCont", "localEltDecl", "arrayEltDecl", "structEltDecl", 
                   "structDecl" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    UP_ONE=23
    ABS=24
    RELATIV=25
    NE=26
    GE=27
    LE=28
    EQ=29
    LS=30
    RS=31
    SUB=32
    ADD=33
    MUL=34
    AND=35
    E_BIG=36
    E_LITTLE=37
    STRING=38
    COMMENT=39
    ID=40
    WS=41
    INT=42

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProtocolContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def protoDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WiresharkGeneratorParser.ProtoDeclContext)
            else:
                return self.getTypedRuleContext(WiresharkGeneratorParser.ProtoDeclContext,i)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_protocol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProtocol" ):
                listener.enterProtocol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProtocol" ):
                listener.exitProtocol(self)




    def protocol(self):

        localctx = WiresharkGeneratorParser.ProtocolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_protocol)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 51 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 50
                self.protoDecl()
                self.state = 53 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WiresharkGeneratorParser.T__1) | (1 << WiresharkGeneratorParser.T__3) | (1 << WiresharkGeneratorParser.T__7) | (1 << WiresharkGeneratorParser.T__10) | (1 << WiresharkGeneratorParser.T__13) | (1 << WiresharkGeneratorParser.T__14) | (1 << WiresharkGeneratorParser.T__21))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ProtoDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def dissectorTableDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.DissectorTableDeclContext,0)


        def endianDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.EndianDeclContext,0)


        def protoDetailsDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.ProtoDetailsDeclContext,0)


        def dissectorEntryDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.DissectorEntryDeclContext,0)


        def enumDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.EnumDeclContext,0)


        def structDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.StructDeclContext,0)


        def typeDef(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.TypeDefContext,0)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_protoDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProtoDecl" ):
                listener.enterProtoDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProtoDecl" ):
                listener.exitProtoDecl(self)




    def protoDecl(self):

        localctx = WiresharkGeneratorParser.ProtoDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_protoDecl)
        try:
            self.state = 76
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WiresharkGeneratorParser.T__3]:
                self.enterOuterAlt(localctx, 1)
                self.state = 55
                self.dissectorTableDecl()
                self.state = 56
                self.match(WiresharkGeneratorParser.T__0)
                pass
            elif token in [WiresharkGeneratorParser.T__1]:
                self.enterOuterAlt(localctx, 2)
                self.state = 58
                self.endianDecl()
                self.state = 59
                self.match(WiresharkGeneratorParser.T__0)
                pass
            elif token in [WiresharkGeneratorParser.T__7]:
                self.enterOuterAlt(localctx, 3)
                self.state = 61
                self.protoDetailsDecl()
                self.state = 62
                self.match(WiresharkGeneratorParser.T__0)
                pass
            elif token in [WiresharkGeneratorParser.T__10]:
                self.enterOuterAlt(localctx, 4)
                self.state = 64
                self.dissectorEntryDecl()
                self.state = 65
                self.match(WiresharkGeneratorParser.T__0)
                pass
            elif token in [WiresharkGeneratorParser.T__13]:
                self.enterOuterAlt(localctx, 5)
                self.state = 67
                self.enumDecl()
                self.state = 68
                self.match(WiresharkGeneratorParser.T__0)
                pass
            elif token in [WiresharkGeneratorParser.T__21]:
                self.enterOuterAlt(localctx, 6)
                self.state = 70
                self.structDecl()
                self.state = 71
                self.match(WiresharkGeneratorParser.T__0)
                pass
            elif token in [WiresharkGeneratorParser.T__14]:
                self.enterOuterAlt(localctx, 7)
                self.state = 73
                self.typeDef()
                self.state = 74
                self.match(WiresharkGeneratorParser.T__0)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EndianDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def E_BIG(self):
            return self.getToken(WiresharkGeneratorParser.E_BIG, 0)

        def E_LITTLE(self):
            return self.getToken(WiresharkGeneratorParser.E_LITTLE, 0)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_endianDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEndianDecl" ):
                listener.enterEndianDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEndianDecl" ):
                listener.exitEndianDecl(self)




    def endianDecl(self):

        localctx = WiresharkGeneratorParser.EndianDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_endianDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 78
            self.match(WiresharkGeneratorParser.T__1)
            self.state = 79
            self.match(WiresharkGeneratorParser.T__2)
            self.state = 80
            _la = self._input.LA(1)
            if not(_la==WiresharkGeneratorParser.E_BIG or _la==WiresharkGeneratorParser.E_LITTLE):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DissectorTableDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.STRING)
            else:
                return self.getToken(WiresharkGeneratorParser.STRING, i)

        def ID(self):
            return self.getToken(WiresharkGeneratorParser.ID, 0)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_dissectorTableDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDissectorTableDecl" ):
                listener.enterDissectorTableDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDissectorTableDecl" ):
                listener.exitDissectorTableDecl(self)




    def dissectorTableDecl(self):

        localctx = WiresharkGeneratorParser.DissectorTableDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_dissectorTableDecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 82
            self.match(WiresharkGeneratorParser.T__3)
            self.state = 83
            self.match(WiresharkGeneratorParser.T__4)
            self.state = 84
            self.match(WiresharkGeneratorParser.STRING)
            self.state = 85
            self.match(WiresharkGeneratorParser.T__5)
            self.state = 86
            self.match(WiresharkGeneratorParser.STRING)
            self.state = 87
            self.match(WiresharkGeneratorParser.T__6)
            self.state = 88
            self.match(WiresharkGeneratorParser.T__2)
            self.state = 89
            self.match(WiresharkGeneratorParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ProtoDetailsDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.STRING)
            else:
                return self.getToken(WiresharkGeneratorParser.STRING, i)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_protoDetailsDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProtoDetailsDecl" ):
                listener.enterProtoDetailsDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProtoDetailsDecl" ):
                listener.exitProtoDetailsDecl(self)




    def protoDetailsDecl(self):

        localctx = WiresharkGeneratorParser.ProtoDetailsDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_protoDetailsDecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 91
            self.match(WiresharkGeneratorParser.T__7)
            self.state = 92
            self.match(WiresharkGeneratorParser.T__2)
            self.state = 93
            self.match(WiresharkGeneratorParser.T__8)
            self.state = 94
            self.match(WiresharkGeneratorParser.STRING)
            self.state = 95
            self.match(WiresharkGeneratorParser.T__5)
            self.state = 96
            self.match(WiresharkGeneratorParser.STRING)
            self.state = 97
            self.match(WiresharkGeneratorParser.T__5)
            self.state = 98
            self.match(WiresharkGeneratorParser.STRING)
            self.state = 99
            self.match(WiresharkGeneratorParser.T__9)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DissectorEntryDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.ID)
            else:
                return self.getToken(WiresharkGeneratorParser.ID, i)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_dissectorEntryDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDissectorEntryDecl" ):
                listener.enterDissectorEntryDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDissectorEntryDecl" ):
                listener.exitDissectorEntryDecl(self)




    def dissectorEntryDecl(self):

        localctx = WiresharkGeneratorParser.DissectorEntryDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_dissectorEntryDecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 101
            self.match(WiresharkGeneratorParser.T__10)
            self.state = 102
            self.match(WiresharkGeneratorParser.ID)
            self.state = 103
            self.match(WiresharkGeneratorParser.T__2)
            self.state = 104
            self.match(WiresharkGeneratorParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EnumEltDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT(self):
            return self.getToken(WiresharkGeneratorParser.INT, 0)

        def ID(self):
            return self.getToken(WiresharkGeneratorParser.ID, 0)

        def STRING(self):
            return self.getToken(WiresharkGeneratorParser.STRING, 0)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_enumEltDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnumEltDecl" ):
                listener.enterEnumEltDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnumEltDecl" ):
                listener.exitEnumEltDecl(self)




    def enumEltDecl(self):

        localctx = WiresharkGeneratorParser.EnumEltDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_enumEltDecl)
        self._la = 0 # Token type
        try:
            self.state = 116
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WiresharkGeneratorParser.INT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 106
                self.match(WiresharkGeneratorParser.INT)
                self.state = 107
                self.match(WiresharkGeneratorParser.T__2)
                self.state = 108
                self.match(WiresharkGeneratorParser.ID)
                self.state = 111
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==WiresharkGeneratorParser.T__11:
                    self.state = 109
                    self.match(WiresharkGeneratorParser.T__11)
                    self.state = 110
                    self.match(WiresharkGeneratorParser.STRING)


                pass
            elif token in [WiresharkGeneratorParser.T__12]:
                self.enterOuterAlt(localctx, 2)
                self.state = 113
                self.match(WiresharkGeneratorParser.T__12)
                self.state = 114
                self.match(WiresharkGeneratorParser.T__2)
                self.state = 115
                self.match(WiresharkGeneratorParser.STRING)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EnumDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.ID)
            else:
                return self.getToken(WiresharkGeneratorParser.ID, i)

        def enumEltDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WiresharkGeneratorParser.EnumEltDeclContext)
            else:
                return self.getTypedRuleContext(WiresharkGeneratorParser.EnumEltDeclContext,i)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_enumDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnumDecl" ):
                listener.enterEnumDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnumDecl" ):
                listener.exitEnumDecl(self)




    def enumDecl(self):

        localctx = WiresharkGeneratorParser.EnumDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_enumDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 118
            self.match(WiresharkGeneratorParser.T__13)
            self.state = 119
            self.match(WiresharkGeneratorParser.ID)
            self.state = 122
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WiresharkGeneratorParser.T__11:
                self.state = 120
                self.match(WiresharkGeneratorParser.T__11)
                self.state = 121
                self.match(WiresharkGeneratorParser.ID)


            self.state = 124
            self.match(WiresharkGeneratorParser.T__8)
            self.state = 125
            self.enumEltDecl()
            self.state = 130
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 126
                    self.match(WiresharkGeneratorParser.T__5)
                    self.state = 127
                    self.enumEltDecl() 
                self.state = 132
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

            self.state = 134
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WiresharkGeneratorParser.T__5:
                self.state = 133
                self.match(WiresharkGeneratorParser.T__5)


            self.state = 136
            self.match(WiresharkGeneratorParser.T__9)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.ID)
            else:
                return self.getToken(WiresharkGeneratorParser.ID, i)

        def INT(self):
            return self.getToken(WiresharkGeneratorParser.INT, 0)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_typeDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeDef" ):
                listener.enterTypeDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeDef" ):
                listener.exitTypeDef(self)




    def typeDef(self):

        localctx = WiresharkGeneratorParser.TypeDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_typeDef)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 138
            self.match(WiresharkGeneratorParser.T__14)
            self.state = 139
            self.match(WiresharkGeneratorParser.ID)
            self.state = 140
            self.match(WiresharkGeneratorParser.ID)
            self.state = 144
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WiresharkGeneratorParser.T__4:
                self.state = 141
                self.match(WiresharkGeneratorParser.T__4)
                self.state = 142
                self.match(WiresharkGeneratorParser.INT)
                self.state = 143
                self.match(WiresharkGeneratorParser.T__6)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParamContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT(self):
            return self.getToken(WiresharkGeneratorParser.INT, 0)

        def STRING(self):
            return self.getToken(WiresharkGeneratorParser.STRING, 0)

        def ID(self):
            return self.getToken(WiresharkGeneratorParser.ID, 0)

        def fieldPath(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.FieldPathContext,0)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_param

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParam" ):
                listener.enterParam(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParam" ):
                listener.exitParam(self)




    def param(self):

        localctx = WiresharkGeneratorParser.ParamContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_param)
        try:
            self.state = 150
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 146
                self.match(WiresharkGeneratorParser.INT)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 147
                self.match(WiresharkGeneratorParser.STRING)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 148
                self.match(WiresharkGeneratorParser.ID)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 149
                self.fieldPath()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParamsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def param(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WiresharkGeneratorParser.ParamContext)
            else:
                return self.getTypedRuleContext(WiresharkGeneratorParser.ParamContext,i)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_params

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParams" ):
                listener.enterParams(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParams" ):
                listener.exitParams(self)




    def params(self):

        localctx = WiresharkGeneratorParser.ParamsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_params)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 152
            self.param()
            self.state = 157
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WiresharkGeneratorParser.T__5:
                self.state = 153
                self.match(WiresharkGeneratorParser.T__5)
                self.state = 154
                self.param()
                self.state = 159
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(WiresharkGeneratorParser.ID, 0)

        def params(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.ParamsContext,0)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_function

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunction" ):
                listener.enterFunction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunction" ):
                listener.exitFunction(self)




    def function(self):

        localctx = WiresharkGeneratorParser.FunctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_function)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 160
            self.match(WiresharkGeneratorParser.ID)

            self.state = 161
            self.match(WiresharkGeneratorParser.T__15)
            self.state = 162
            self.params()
            self.state = 163
            self.match(WiresharkGeneratorParser.T__16)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CaseDeclDetailsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(WiresharkGeneratorParser.ID, 0)

        def structEltDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WiresharkGeneratorParser.StructEltDeclContext)
            else:
                return self.getTypedRuleContext(WiresharkGeneratorParser.StructEltDeclContext,i)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_caseDeclDetails

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCaseDeclDetails" ):
                listener.enterCaseDeclDetails(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCaseDeclDetails" ):
                listener.exitCaseDeclDetails(self)




    def caseDeclDetails(self):

        localctx = WiresharkGeneratorParser.CaseDeclDetailsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_caseDeclDetails)
        self._la = 0 # Token type
        try:
            self.state = 178
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 165
                self.match(WiresharkGeneratorParser.ID)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 166
                self.structEltDecl()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 167
                self.match(WiresharkGeneratorParser.T__8)
                self.state = 171 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 168
                    self.structEltDecl()
                    self.state = 169
                    self.match(WiresharkGeneratorParser.T__0)
                    self.state = 173 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WiresharkGeneratorParser.T__19) | (1 << WiresharkGeneratorParser.T__20) | (1 << WiresharkGeneratorParser.ID))) != 0)):
                        break

                self.state = 175
                self.match(WiresharkGeneratorParser.T__9)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 177
                self.match(WiresharkGeneratorParser.T__17)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CaseLabelContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(WiresharkGeneratorParser.ID, 0)

        def INT(self):
            return self.getToken(WiresharkGeneratorParser.INT, 0)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_caseLabel

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCaseLabel" ):
                listener.enterCaseLabel(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCaseLabel" ):
                listener.exitCaseLabel(self)




    def caseLabel(self):

        localctx = WiresharkGeneratorParser.CaseLabelContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_caseLabel)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 180
            _la = self._input.LA(1)
            if not(_la==WiresharkGeneratorParser.ID or _la==WiresharkGeneratorParser.INT):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CaseDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def caseDeclDetails(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.CaseDeclDetailsContext,0)


        def caseLabel(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.CaseLabelContext,0)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_caseDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCaseDecl" ):
                listener.enterCaseDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCaseDecl" ):
                listener.exitCaseDecl(self)




    def caseDecl(self):

        localctx = WiresharkGeneratorParser.CaseDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_caseDecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 185
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WiresharkGeneratorParser.T__18]:
                self.state = 182
                self.match(WiresharkGeneratorParser.T__18)
                self.state = 183
                self.caseLabel()
                pass
            elif token in [WiresharkGeneratorParser.T__12]:
                self.state = 184
                self.match(WiresharkGeneratorParser.T__12)
                pass
            else:
                raise NoViableAltException(self)

            self.state = 187
            self.match(WiresharkGeneratorParser.T__11)
            self.state = 188
            self.caseDeclDetails()
            self.state = 189
            self.match(WiresharkGeneratorParser.T__0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FieldContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(WiresharkGeneratorParser.ID, 0)

        def STRING(self):
            return self.getToken(WiresharkGeneratorParser.STRING, 0)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_field

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterField" ):
                listener.enterField(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitField" ):
                listener.exitField(self)




    def field(self):

        localctx = WiresharkGeneratorParser.FieldContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_field)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 191
            _la = self._input.LA(1)
            if not(_la==WiresharkGeneratorParser.STRING or _la==WiresharkGeneratorParser.ID):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FieldPathContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.startSym = None # Token

        def field(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WiresharkGeneratorParser.FieldContext)
            else:
                return self.getTypedRuleContext(WiresharkGeneratorParser.FieldContext,i)


        def ABS(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.ABS)
            else:
                return self.getToken(WiresharkGeneratorParser.ABS, i)

        def RELATIV(self):
            return self.getToken(WiresharkGeneratorParser.RELATIV, 0)

        def UP_ONE(self):
            return self.getToken(WiresharkGeneratorParser.UP_ONE, 0)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_fieldPath

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFieldPath" ):
                listener.enterFieldPath(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFieldPath" ):
                listener.exitFieldPath(self)




    def fieldPath(self):

        localctx = WiresharkGeneratorParser.FieldPathContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_fieldPath)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 194
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WiresharkGeneratorParser.UP_ONE) | (1 << WiresharkGeneratorParser.ABS) | (1 << WiresharkGeneratorParser.RELATIV))) != 0):
                self.state = 193
                localctx.startSym = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WiresharkGeneratorParser.UP_ONE) | (1 << WiresharkGeneratorParser.ABS) | (1 << WiresharkGeneratorParser.RELATIV))) != 0)):
                    localctx.startSym = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()


            self.state = 196
            self.field()
            self.state = 201
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WiresharkGeneratorParser.ABS:
                self.state = 197
                self.match(WiresharkGeneratorParser.ABS)
                self.state = 198
                self.field()
                self.state = 203
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SwitchStructEltCtrlContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.op = None # Token

        def fieldPath(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.FieldPathContext,0)


        def INT(self):
            return self.getToken(WiresharkGeneratorParser.INT, 0)

        def ID(self):
            return self.getToken(WiresharkGeneratorParser.ID, 0)

        def NE(self):
            return self.getToken(WiresharkGeneratorParser.NE, 0)

        def GE(self):
            return self.getToken(WiresharkGeneratorParser.GE, 0)

        def LE(self):
            return self.getToken(WiresharkGeneratorParser.LE, 0)

        def EQ(self):
            return self.getToken(WiresharkGeneratorParser.EQ, 0)

        def LS(self):
            return self.getToken(WiresharkGeneratorParser.LS, 0)

        def RS(self):
            return self.getToken(WiresharkGeneratorParser.RS, 0)

        def ADD(self):
            return self.getToken(WiresharkGeneratorParser.ADD, 0)

        def SUB(self):
            return self.getToken(WiresharkGeneratorParser.SUB, 0)

        def AND(self):
            return self.getToken(WiresharkGeneratorParser.AND, 0)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_switchStructEltCtrl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSwitchStructEltCtrl" ):
                listener.enterSwitchStructEltCtrl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSwitchStructEltCtrl" ):
                listener.exitSwitchStructEltCtrl(self)




    def switchStructEltCtrl(self):

        localctx = WiresharkGeneratorParser.SwitchStructEltCtrlContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_switchStructEltCtrl)
        self._la = 0 # Token type
        try:
            self.state = 209
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 204
                self.fieldPath()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 205
                self.fieldPath()
                self.state = 206
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WiresharkGeneratorParser.NE) | (1 << WiresharkGeneratorParser.GE) | (1 << WiresharkGeneratorParser.LE) | (1 << WiresharkGeneratorParser.EQ) | (1 << WiresharkGeneratorParser.LS) | (1 << WiresharkGeneratorParser.RS) | (1 << WiresharkGeneratorParser.SUB) | (1 << WiresharkGeneratorParser.ADD) | (1 << WiresharkGeneratorParser.AND))) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 207
                _la = self._input.LA(1)
                if not(_la==WiresharkGeneratorParser.ID or _la==WiresharkGeneratorParser.INT):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SwitchDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def switchStructEltCtrl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.SwitchStructEltCtrlContext,0)


        def caseDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WiresharkGeneratorParser.CaseDeclContext)
            else:
                return self.getTypedRuleContext(WiresharkGeneratorParser.CaseDeclContext,i)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_switchDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSwitchDecl" ):
                listener.enterSwitchDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSwitchDecl" ):
                listener.exitSwitchDecl(self)




    def switchDecl(self):

        localctx = WiresharkGeneratorParser.SwitchDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_switchDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 211
            self.match(WiresharkGeneratorParser.T__19)
            self.state = 212
            self.match(WiresharkGeneratorParser.T__15)
            self.state = 213
            self.switchStructEltCtrl()
            self.state = 214
            self.match(WiresharkGeneratorParser.T__16)
            self.state = 215
            self.match(WiresharkGeneratorParser.T__8)
            self.state = 217 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 216
                self.caseDecl()
                self.state = 219 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==WiresharkGeneratorParser.T__12 or _la==WiresharkGeneratorParser.T__18):
                    break

            self.state = 221
            self.match(WiresharkGeneratorParser.T__9)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExternEltDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.STRING)
            else:
                return self.getToken(WiresharkGeneratorParser.STRING, i)

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.ID)
            else:
                return self.getToken(WiresharkGeneratorParser.ID, i)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_externEltDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExternEltDecl" ):
                listener.enterExternEltDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExternEltDecl" ):
                listener.exitExternEltDecl(self)




    def externEltDecl(self):

        localctx = WiresharkGeneratorParser.ExternEltDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_externEltDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 223
            self.match(WiresharkGeneratorParser.T__20)
            self.state = 224
            self.match(WiresharkGeneratorParser.T__4)
            self.state = 225
            self.match(WiresharkGeneratorParser.STRING)
            self.state = 226
            self.match(WiresharkGeneratorParser.T__6)
            self.state = 227
            self.match(WiresharkGeneratorParser.ID)
            self.state = 228
            _la = self._input.LA(1)
            if not(_la==WiresharkGeneratorParser.STRING or _la==WiresharkGeneratorParser.ID):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LocalEltDeclContContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.INT)
            else:
                return self.getToken(WiresharkGeneratorParser.INT, i)

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.ID)
            else:
                return self.getToken(WiresharkGeneratorParser.ID, i)

        def STRING(self):
            return self.getToken(WiresharkGeneratorParser.STRING, 0)

        def SUB(self):
            return self.getToken(WiresharkGeneratorParser.SUB, 0)

        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_localEltDeclCont

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLocalEltDeclCont" ):
                listener.enterLocalEltDeclCont(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLocalEltDeclCont" ):
                listener.exitLocalEltDeclCont(self)




    def localEltDeclCont(self):

        localctx = WiresharkGeneratorParser.LocalEltDeclContContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_localEltDeclCont)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 230
            self.match(WiresharkGeneratorParser.T__11)
            self.state = 231
            self.match(WiresharkGeneratorParser.INT)
            self.state = 234
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WiresharkGeneratorParser.SUB:
                self.state = 232
                self.match(WiresharkGeneratorParser.SUB)
                self.state = 233
                self.match(WiresharkGeneratorParser.INT)


            self.state = 236
            self.match(WiresharkGeneratorParser.T__11)
            self.state = 237
            self.match(WiresharkGeneratorParser.ID)
            self.state = 238
            _la = self._input.LA(1)
            if not(_la==WiresharkGeneratorParser.STRING or _la==WiresharkGeneratorParser.ID):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LocalEltDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.ID)
            else:
                return self.getToken(WiresharkGeneratorParser.ID, i)

        def STRING(self):
            return self.getToken(WiresharkGeneratorParser.STRING, 0)

        def localEltDeclCont(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WiresharkGeneratorParser.LocalEltDeclContContext)
            else:
                return self.getTypedRuleContext(WiresharkGeneratorParser.LocalEltDeclContContext,i)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_localEltDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLocalEltDecl" ):
                listener.enterLocalEltDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLocalEltDecl" ):
                listener.exitLocalEltDecl(self)




    def localEltDecl(self):

        localctx = WiresharkGeneratorParser.LocalEltDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_localEltDecl)
        self._la = 0 # Token type
        try:
            self.state = 251
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 240
                self.match(WiresharkGeneratorParser.ID)
                self.state = 241
                _la = self._input.LA(1)
                if not(_la==WiresharkGeneratorParser.STRING or _la==WiresharkGeneratorParser.ID):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 242
                self.match(WiresharkGeneratorParser.ID)
                self.state = 243
                self.localEltDeclCont()
                self.state = 248
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WiresharkGeneratorParser.T__5:
                    self.state = 244
                    self.match(WiresharkGeneratorParser.T__5)
                    self.state = 245
                    self.localEltDeclCont()
                    self.state = 250
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArrayEltDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(WiresharkGeneratorParser.ID)
            else:
                return self.getToken(WiresharkGeneratorParser.ID, i)

        def STRING(self):
            return self.getToken(WiresharkGeneratorParser.STRING, 0)

        def INT(self):
            return self.getToken(WiresharkGeneratorParser.INT, 0)

        def switchStructEltCtrl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.SwitchStructEltCtrlContext,0)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_arrayEltDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArrayEltDecl" ):
                listener.enterArrayEltDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArrayEltDecl" ):
                listener.exitArrayEltDecl(self)




    def arrayEltDecl(self):

        localctx = WiresharkGeneratorParser.ArrayEltDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_arrayEltDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 253
            self.match(WiresharkGeneratorParser.ID)
            self.state = 254
            _la = self._input.LA(1)
            if not(_la==WiresharkGeneratorParser.STRING or _la==WiresharkGeneratorParser.ID):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 255
            self.match(WiresharkGeneratorParser.T__4)
            self.state = 258
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WiresharkGeneratorParser.INT]:
                self.state = 256
                self.match(WiresharkGeneratorParser.INT)
                pass
            elif token in [WiresharkGeneratorParser.UP_ONE, WiresharkGeneratorParser.ABS, WiresharkGeneratorParser.RELATIV, WiresharkGeneratorParser.STRING, WiresharkGeneratorParser.ID]:
                self.state = 257
                self.switchStructEltCtrl()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 260
            self.match(WiresharkGeneratorParser.T__6)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StructEltDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def externEltDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.ExternEltDeclContext,0)


        def function(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.FunctionContext,0)


        def localEltDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.LocalEltDeclContext,0)


        def arrayEltDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.ArrayEltDeclContext,0)


        def switchDecl(self):
            return self.getTypedRuleContext(WiresharkGeneratorParser.SwitchDeclContext,0)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_structEltDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStructEltDecl" ):
                listener.enterStructEltDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStructEltDecl" ):
                listener.exitStructEltDecl(self)




    def structEltDecl(self):

        localctx = WiresharkGeneratorParser.StructEltDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_structEltDecl)
        try:
            self.state = 267
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,21,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 262
                self.externEltDecl()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 263
                self.function()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 264
                self.localEltDecl()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 265
                self.arrayEltDecl()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 266
                self.switchDecl()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StructDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(WiresharkGeneratorParser.ID, 0)

        def structEltDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WiresharkGeneratorParser.StructEltDeclContext)
            else:
                return self.getTypedRuleContext(WiresharkGeneratorParser.StructEltDeclContext,i)


        def getRuleIndex(self):
            return WiresharkGeneratorParser.RULE_structDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStructDecl" ):
                listener.enterStructDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStructDecl" ):
                listener.exitStructDecl(self)




    def structDecl(self):

        localctx = WiresharkGeneratorParser.StructDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_structDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 269
            self.match(WiresharkGeneratorParser.T__21)
            self.state = 270
            self.match(WiresharkGeneratorParser.ID)
            self.state = 271
            self.match(WiresharkGeneratorParser.T__8)
            self.state = 275 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 272
                self.structEltDecl()
                self.state = 273
                self.match(WiresharkGeneratorParser.T__0)
                self.state = 277 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WiresharkGeneratorParser.T__19) | (1 << WiresharkGeneratorParser.T__20) | (1 << WiresharkGeneratorParser.ID))) != 0)):
                    break

            self.state = 279
            self.match(WiresharkGeneratorParser.T__9)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





