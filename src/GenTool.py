#!/usr/bin/python3 
# The main code for generating the various tools

import sys
import argparse
from os import access, R_OK
from os.path import isfile
from antlr4 import *
from WiresharkGeneratorLexer import WiresharkGeneratorLexer
from WiresharkGeneratorParser import WiresharkGeneratorParser
from WiresharkGeneratorListener import WiresharkGeneratorListener
from antlr4.error.ErrorListener import ErrorListener
from antlr4.error.ErrorListener import ConsoleErrorListener
from GenASTListener import WiresharkASTListener
from GenCDissector import GenCDissector
from GenLuaDissector import GenLuaDissector
from GenPacketTool import GenPacketTool

INP_DEFAULT = '<<<stdin>>>'
OUT_DEFAULT = '<<<stdout>>>'
C_TOOL = 'c'
LUA_TOOL = 'l'
PACKET_TOOL = 'p'

verbosity = 0
log_file = sys.stderr

def log_it(val_str):
    if verbosity > 0:
        log_file.write(val_str)
        log_file.write('\n')

class GeneratorErrorListener(ErrorListener):
    def __init__(self, listener):
        super().__init__()
        self.listener = listener

    def systaxError(self, recognizer, offendingSymbol, line, col, msg, e):
        log_it("Syntax error at line {} col {}: {}".format(line, col, msg))

def main(argv):
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-v', '--verbosity', action='count',
                            help='Increase output verbosity',
                            default=0)
    arg_parser.add_argument('-t', '--tool',
                            choices=['C', 'c', 'L', 'l', 'P', 'p'],
                            help='Generate a tool. C/c for C dissector. L/l for Lua dissector. P/p for packet generator. Default is C/c.',
                            default=C_TOOL)
    arg_parser.add_argument('input', nargs='?', default=INP_DEFAULT)
    arg_parser.add_argument('output', nargs='?', default=OUT_DEFAULT)
    args = arg_parser.parse_args()

    global verbosity
    verbosity = args.verbosity # Nothing before here can be logged
    log_it("Verbosity: {}".format(args.verbosity))
    log_it("Tool:      {}".format(args.tool))
    log_it("Input:     {}".format(args.input))
    log_it("Output:    {}".format(args.output))

    if args.input == INP_DEFAULT:
        input_stream = StdinStream()
    else:
        # Check if it exists and is readable
        if isfile(args.input) and access(args.input, R_OK):
            input_stream = FileStream(args.input)
        else:
            print("File {} doesn't exist or is not readable".format(args.input))
            sys.exit()

    lexer = WiresharkGeneratorLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = WiresharkGeneratorParser(stream)

    listener = WiresharkASTListener(args.verbosity)

    # Set up new error listener
    parser.removeErrorListeners()
    parser.addErrorListener(GeneratorErrorListener(listener))

    tree = parser.protocol()

    walker = ParseTreeWalker()

    if str.lower(args.tool) == C_TOOL:
        generator = GenCDissector(log_it, listener, args.verbosity)
    elif str.lower(args.tool) == LUA_TOOL:
        generator = GenLuaDissector(log_it, listener, args.verbosity)
    elif str.lower(args.tool) == PACKET_TOOL:
        generator = GenPacketTool(log_it, listener, args.verbosity)

    walker.walk(listener, tree)

    # Now, generate the code
    if not listener.getSemanticErrors() and not listener.getSyntaxErrors():
        generator.generateCode()
    else:
        for error in listener.getErrorStrings():
            print(error)

if __name__ == '__main__':
    main(sys.argv)
